#!/bin/bash

set -e
set -x

script_dir=$(dirname "$0")

if [ -z "$1" ]
then
  src_dir=$script_dir/../src
else
  src_dir=${1}
fi

if [ -z "$2" ]
then
  dist_dir=${script_dir}/../public
else
  dist_dir=${2}
fi

mkdir -p $dist_dir
rsync -aH $src_dir/ $dist_dir

function archive_n_pages() {
  pushd ${dist_dir}
  find -L . -type f -name "*.npages" | sort | xargs -d "\n" tar -cvzf npages.tar.gz
  popd
}

function n_pages() {
  if [ -z "$1" ]
  then
    >&2 echo "Require one (1) argument <input-pdf>" >&2
  else
    pdfinfo "${1}" | awk '/^Pages:/ {print $2}' | tr -d '[:space:]' > "${1}.npages"
  fi
}

function split_pdf() {
  if [ -z "$3" ]
  then
    >&2 echo "Require three (3) arguments <input-pdf> <pages> <output-pdf>" >&2
  else
    pdftk "${1}" cat "${2}" output "${dist_dir}/${3}"
    n_pages "${dist_dir}/${3}"
  fi
}

function build_vhysa_normal_procedures() {
  # Split VH-YSA normal procedures
  vh_ysa_normal_procedures_path="vh-ysa/normal-procedures"
  vh_ysa_normal_procedures="${dist_dir}/${vh_ysa_normal_procedures_path}"
  vh_ysa_normal_procedures_pdf="${vh_ysa_normal_procedures}.pdf"
  if [ -f "${vh_ysa_normal_procedures_pdf}" ]; then
    n_pages "${vh_ysa_normal_procedures_pdf}"
  fi
}

function build_vhysa_emergency_procedures() {
  # Split VH-YSA emergency procedures
  vh_ysa_emergency_procedures_path="vh-ysa/emergency-procedures"
  vh_ysa_emergency_procedures="${dist_dir}/${vh_ysa_emergency_procedures_path}"
  vh_ysa_emergency_procedures_pdf="${vh_ysa_emergency_procedures}.pdf"
  if [ -f "${vh_ysa_emergency_procedures_pdf}" ]; then
    n_pages "${vh_ysa_emergency_procedures_pdf}"
  fi
}

function build_vhnqx_manual() {
  # Split VH-NQX manual
  vh_nqx_manual_path="vh-nqx/8kcab_vh-nqx"
  vh_nqx_manual="${dist_dir}/${vh_nqx_manual_path}"
  vh_nqx_manual_pdf="${vh_nqx_manual}.pdf"
  if [ -f "${vh_nqx_manual_pdf}" ]; then
    n_pages "${vh_nqx_manual_pdf}"
  fi
}

function build_aquila_a210_manual() {
  # Aquila A210 manual
  aquila_a210_factory_manual_path="FM_AT01_1010_100E_B04____SB_AT01_029_02-AFM_page_1-8_2-4_5"
  aquila_a210_factory_manual="${dist_dir}/${aquila_a210_factory_manual_path}"
  aquila_a210_factory_manual_pdf="${aquila_a210_factory_manual}.pdf"
  # apply supplement SB-AT01-029 (B.01) to FM_AT01_1010_100E_B04 (B.04)
  if [ -f "${dist_dir}/FM_AT01_1010_100E_B04.pdf" ] && [ -f "${dist_dir}/SB_AT01_029_02-AFM_page_1-8_2-4_5.pdf" ]; then
    pdftk A=${dist_dir}/FM_AT01_1010_100E_B04.pdf B=${dist_dir}/SB_AT01_029_02-AFM_page_1-8_2-4_5.pdf cat A1-16 B1 A18-24 B2 B3 A27-end output ${aquila_a210_factory_manual_pdf}
    n_pages "${dist_dir}/FM_AT01_1010_100E_B04.pdf"
    n_pages "${dist_dir}/SB_AT01_029_02-AFM_page_1-8_2-4_5.pdf"
    n_pages ${aquila_a210_factory_manual_pdf}
  fi

  # Split Aquila A210 Factory Manual
  if [ -f "${aquila_a210_factory_manual_pdf}" ]; then
    split_pdf ${aquila_a210_factory_manual_pdf} 1 ${aquila_a210_factory_manual_path}_section0.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 10-21 ${aquila_a210_factory_manual_path}_section1_general.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 22-41 ${aquila_a210_factory_manual_path}_section2_limitations.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 42-57 ${aquila_a210_factory_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 58-77 ${aquila_a210_factory_manual_path}_section4_normal_procedures.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 78-110 ${aquila_a210_factory_manual_path}_section5_performance.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 111-124 ${aquila_a210_factory_manual_path}_section6_weight_and_balance.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 125-156 ${aquila_a210_factory_manual_path}_section7_system_description.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 157-164 ${aquila_a210_factory_manual_path}_section8_handling_service_maintenance.pdf
    split_pdf ${aquila_a210_factory_manual_pdf} 165-167 ${aquila_a210_factory_manual_path}_section9_supplements.pdf
  fi
}

function build_vhbne_manual() {
  # Split VH-BNE manual
  vh_bne_manual_path="vh-bne/aquila-a210_vh-bne"
  vh_bne_manual="${dist_dir}/${vh_bne_manual_path}"
  vh_bne_manual_pdf="${vh_bne_manual}.pdf"
  if [ -f "${vh_bne_manual_pdf}" ]; then
    n_pages ${vh_bne_manual_pdf}
    split_pdf ${vh_bne_manual_pdf} 1 ${vh_bne_manual_path}_cover.pdf
    split_pdf ${vh_bne_manual_pdf} 2-9 ${vh_bne_manual_path}_introduction.pdf
    split_pdf ${vh_bne_manual_pdf} 10-21 ${vh_bne_manual_path}_section1_general.pdf
    split_pdf ${vh_bne_manual_pdf} 22-46 ${vh_bne_manual_path}_section2_limitations.pdf
    split_pdf ${vh_bne_manual_pdf} 47-62 ${vh_bne_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${vh_bne_manual_pdf} 63-82 ${vh_bne_manual_path}_section4_normal_procedures.pdf
    split_pdf ${vh_bne_manual_pdf} 83-117 ${vh_bne_manual_path}_section5_performance.pdf
    split_pdf ${vh_bne_manual_pdf} 118-132 ${vh_bne_manual_path}_section6_weight_and_balance_equipment_list.pdf
    split_pdf ${vh_bne_manual_pdf} 133-164 ${vh_bne_manual_path}_section7_description_of_the_aircraft_and_its_systems.pdf
    split_pdf ${vh_bne_manual_pdf} 165-172 ${vh_bne_manual_path}_section8_handling_service_maintenance.pdf
    split_pdf ${vh_bne_manual_pdf} 173-254 ${vh_bne_manual_path}_section9_supplements.pdf
    split_pdf ${vh_bne_manual_pdf} 178-183 ${vh_bne_manual_path}_section9_supplements_ave4.pdf
    split_pdf ${vh_bne_manual_pdf} 184-189 ${vh_bne_manual_path}_section9_supplements_ave5.pdf
    split_pdf ${vh_bne_manual_pdf} 190-193 ${vh_bne_manual_path}_section9_supplements_ave10.pdf
    split_pdf ${vh_bne_manual_pdf} 194-203 ${vh_bne_manual_path}_section9_supplements_ave11.pdf
    split_pdf ${vh_bne_manual_pdf} 204-214 ${vh_bne_manual_path}_section9_supplements_ave12.pdf
    split_pdf ${vh_bne_manual_pdf} 215-226 ${vh_bne_manual_path}_section9_supplements_ave16.pdf
    split_pdf ${vh_bne_manual_pdf} 227-244 ${vh_bne_manual_path}_section9_supplements_ave23.pdf
    split_pdf ${vh_bne_manual_pdf} 245-254 ${vh_bne_manual_path}_section9_supplements_ave26.pdf
  fi
}

function build_sling2_factory_manual() {
  # Split Sling 2 Factory Manual
  sling_2_factory_manual_path="Sling-2-POH-2.3-RSA-operating_handbook"
  sling_2_factory_manual="${dist_dir}/${sling_2_factory_manual_path}"
  sling_2_factory_manual_pdf="${sling_2_factory_manual}.pdf"
  if [ -f "${sling_2_factory_manual_pdf}" ]; then
    n_pages ${sling_2_factory_manual_pdf}
    split_pdf ${sling_2_factory_manual_pdf} 1 ${sling_2_factory_manual_path}_cover.pdf
    split_pdf ${sling_2_factory_manual_pdf} 2-9 ${sling_2_factory_manual_path}_introduction.pdf
    split_pdf ${sling_2_factory_manual_pdf} 10 ${sling_2_factory_manual_path}_table_of_contents.pdf
    split_pdf ${sling_2_factory_manual_pdf} 11-28 ${sling_2_factory_manual_path}_1_general_information.pdf
    split_pdf ${sling_2_factory_manual_pdf} 29-44 ${sling_2_factory_manual_path}_2_limitations.pdf
    split_pdf ${sling_2_factory_manual_pdf} 45-64 ${sling_2_factory_manual_path}_3_emergency_procedures.pdf
    split_pdf ${sling_2_factory_manual_pdf} 65-85 ${sling_2_factory_manual_path}_4_normal_procedures.pdf
    split_pdf ${sling_2_factory_manual_pdf} 86-91 ${sling_2_factory_manual_path}_5_performance.pdf
    split_pdf ${sling_2_factory_manual_pdf} 92-99 ${sling_2_factory_manual_path}_6_weight_and_balance.pdf
    split_pdf ${sling_2_factory_manual_pdf} 100-134 ${sling_2_factory_manual_path}_7_systems.pdf
    split_pdf ${sling_2_factory_manual_pdf} 135-142 ${sling_2_factory_manual_path}_8_airplane_ground_handling_service.pdf
    split_pdf ${sling_2_factory_manual_pdf} 143-192 ${sling_2_factory_manual_path}_9_supplementary_information.pdf
  fi
}

function build_244844_manual() {
  # Split 24-4844 Manual
  eurofox_24_4844_manual_path="24-4844/eurofox-3k_24-4844"
  eurofox_24_4844_manual="${dist_dir}/${eurofox_24_4844_manual_path}"
  eurofox_24_4844_manual_pdf="${eurofox_24_4844_manual}.pdf"
  if [ -f "${eurofox_24_4844_manual_pdf}" ]; then
    n_pages ${eurofox_24_4844_manual_pdf}
    split_pdf ${eurofox_24_4844_manual_pdf} 1 ${eurofox_24_4844_manual_path}_cover.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 2-4 ${eurofox_24_4844_manual_path}_introduction.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 5-8 ${eurofox_24_4844_manual_path}_table_of_contents.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 9 ${eurofox_24_4844_manual_path}_general_information.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 10-21 ${eurofox_24_4844_manual_path}_airplane_and_systems_description.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 22-25 ${eurofox_24_4844_manual_path}_operating_limitations.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 26-29 ${eurofox_24_4844_manual_path}_weight_and_balance_information.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 30-32 ${eurofox_24_4844_manual_path}_performance.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 33-41 ${eurofox_24_4844_manual_path}_emergency_procedures.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 42-57 ${eurofox_24_4844_manual_path}_normal_procedures.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 58-60 ${eurofox_24_4844_manual_path}_aircraft_ground_handling_and_servicing.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 61-63 ${eurofox_24_4844_manual_path}_required_placards_and_markings.pdf
    split_pdf ${eurofox_24_4844_manual_pdf} 64 ${eurofox_24_4844_manual_path}_supplementary_information.pdf
  fi
}

function build_245350_manual() {
  # Split 24-5350 Manual
  eurofox_24_5350_manual_path="24-5350/eurofox-3k_24-5350"
  eurofox_24_5350_manual="${dist_dir}/${eurofox_24_5350_manual_path}"
  eurofox_24_5350_manual_pdf="${eurofox_24_5350_manual}.pdf"
  if [ -f "${eurofox_24_5350_manual_pdf}" ]; then
    n_pages ${eurofox_24_5350_manual_pdf}
    split_pdf ${eurofox_24_5350_manual_pdf} 1 ${eurofox_24_5350_manual_path}_cover.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 2-3 ${eurofox_24_5350_manual_path}_introduction.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 4-7 ${eurofox_24_5350_manual_path}_table_of_contents.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 8 ${eurofox_24_5350_manual_path}_general_information.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 9-20 ${eurofox_24_5350_manual_path}_airplane_and_systems_description.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 21-24 ${eurofox_24_5350_manual_path}_operating_limitations.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 25-29 ${eurofox_24_5350_manual_path}_weight_and_balance_information.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 30-32 ${eurofox_24_5350_manual_path}_performance.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 33-41 ${eurofox_24_5350_manual_path}_emergency_procedures.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 42-57 ${eurofox_24_5350_manual_path}_normal_procedures.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 58-60 ${eurofox_24_5350_manual_path}_aircraft_ground_handling_and_servicing.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 61-63 ${eurofox_24_5350_manual_path}_required_placards_and_markings.pdf
    split_pdf ${eurofox_24_5350_manual_pdf} 64 ${eurofox_24_5350_manual_path}_supplementary_information.pdf
  fi
}

function build_248881_manual() {
  # Split 24-8881 Manual
  eurofox_24_8881_manual_path="24-8881/eurofox-3k_24-8881"
  eurofox_24_8881_manual="${dist_dir}/${eurofox_24_8881_manual_path}"
  eurofox_24_8881_manual_pdf="${eurofox_24_8881_manual}.pdf"
  if [ -f "${eurofox_24_8881_manual_pdf}" ]; then
    n_pages ${eurofox_24_8881_manual_pdf}
    split_pdf ${eurofox_24_8881_manual_pdf} 1 ${eurofox_24_8881_manual_path}_cover.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 2-10 ${eurofox_24_8881_manual_path}_introduction.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 11-14 ${eurofox_24_8881_manual_path}_table_of_contents.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 15-16 ${eurofox_24_8881_manual_path}_general_information.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 17-28 ${eurofox_24_8881_manual_path}_airplane_and_systems_description.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 29-32 ${eurofox_24_8881_manual_path}_operating_limitations.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 33-39 ${eurofox_24_8881_manual_path}_weight_and_balance_information.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 40-42 ${eurofox_24_8881_manual_path}_performance.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 43-51 ${eurofox_24_8881_manual_path}_emergency_procedures.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 52-67 ${eurofox_24_8881_manual_path}_normal_procedures.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 68-69 ${eurofox_24_8881_manual_path}_aircraft_ground_handling_and_servicing.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 70-72 ${eurofox_24_8881_manual_path}_required_placards_and_markings.pdf
    split_pdf ${eurofox_24_8881_manual_pdf} 73 ${eurofox_24_8881_manual_path}_supplementary_information.pdf
  fi
}

function build_eurofox_3k_factory_manual() {
  # Split Eurofox 3K Factory Manual
  eurofox_3k_factory_manual_path="a240-poh"
  eurofox_3k_factory_manual="${dist_dir}/${eurofox_3k_factory_manual_path}"
  eurofox_3k_factory_manual_pdf="${eurofox_3k_factory_manual}.pdf"
  if [ -f "${eurofox_3k_factory_manual_pdf}" ]; then
    n_pages ${eurofox_3k_factory_manual_pdf}
    split_pdf ${eurofox_3k_factory_manual_pdf} 1 ${eurofox_3k_factory_manual_path}_cover.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 4-6 ${eurofox_3k_factory_manual_path}_table_of_contents.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 7-7 ${eurofox_3k_factory_manual_path}_general_information.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 8-17 ${eurofox_3k_factory_manual_path}_airplane_and_systems_description.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 18-20 ${eurofox_3k_factory_manual_path}_operating_limitations.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 21-23 ${eurofox_3k_factory_manual_path}_weight_and_balance_information.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 24-25 ${eurofox_3k_factory_manual_path}_performance.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 26-34 ${eurofox_3k_factory_manual_path}_normal_procedures.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 35-40 ${eurofox_3k_factory_manual_path}_emergency_procedures.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 41-44 ${eurofox_3k_factory_manual_path}_aircraft_ground_handling_and_servicing.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 45-45 ${eurofox_3k_factory_manual_path}_supplementary_information.pdf
    split_pdf ${eurofox_3k_factory_manual_pdf} 46-50 ${eurofox_3k_factory_manual_path}_flight_training_supplement.pdf
  fi
}

function build_eurofox_3k_maintenance_manual() {
  # Split Eurofox 3K Maintenance Manual
  eurofox_3k_maintenance_manual_path="maintenance-manual"
  eurofox_3k_maintenance_manual="${dist_dir}/${eurofox_3k_maintenance_manual_path}"
  eurofox_3k_maintenance_manual_pdf="${eurofox_3k_maintenance_manual}.pdf"
  if [ -f "${eurofox_3k_maintenance_manual_pdf}" ]; then
    n_pages ${eurofox_3k_maintenance_manual_pdf}
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 1 ${eurofox_3k_maintenance_manual_path}_cover.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 2 ${eurofox_3k_maintenance_manual_path}_list_of_effective_pages.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 3 ${eurofox_3k_maintenance_manual_path}_table_of_contents.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 4 ${eurofox_3k_maintenance_manual_path}_foreword.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 5 ${eurofox_3k_maintenance_manual_path}_common_conversions_and_abbreviations.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 6-13 ${eurofox_3k_maintenance_manual_path}_section1_general_description.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 14-24 ${eurofox_3k_maintenance_manual_path}_section2_ground_handling_servicing_lubrication_and_inspection.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 25-33 ${eurofox_3k_maintenance_manual_path}_section3_structures_fuselage.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 34-41 ${eurofox_3k_maintenance_manual_path}_section4_structures_wings_and_empennage.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 42-54 ${eurofox_3k_maintenance_manual_path}_section5_structures_landing_gear_and_brakes_tricycle_gear.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 55-61 ${eurofox_3k_maintenance_manual_path}_section6_structures_aileron_and_flap_control_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 62-65 ${eurofox_3k_maintenance_manual_path}_section7_structures_elevator_control_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 66-69 ${eurofox_3k_maintenance_manual_path}_section8_structures_elevator_trim_control_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 70-72 ${eurofox_3k_maintenance_manual_path}_section9_structures_rudder_control_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 72-94 ${eurofox_3k_maintenance_manual_path}_section10_engine.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 95-101 ${eurofox_3k_maintenance_manual_path}_section11_fuel_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 102-103 ${eurofox_3k_maintenance_manual_path}_section12_propeller.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 104-106 ${eurofox_3k_maintenance_manual_path}_section13_utility_systems.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 107-114 ${eurofox_3k_maintenance_manual_path}_section14_instruments_and_instrument_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 115-123 ${eurofox_3k_maintenance_manual_path}_section15_electrical_systems.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 124 ${eurofox_3k_maintenance_manual_path}_section16_structural_repair.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 125 ${eurofox_3k_maintenance_manual_path}_section17_exterior_painting.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 126 ${eurofox_3k_maintenance_manual_path}_section18_wiring_diagrams_general_wiring_scheme.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 127-128 ${eurofox_3k_maintenance_manual_path}_section19_safety_directives_and_safety_monitoring_system.pdf
    split_pdf ${eurofox_3k_maintenance_manual_pdf} 129-141 ${eurofox_3k_maintenance_manual_path}_a220_taildragger_supplement.pdf
  fi
}

function build_eurofox_3k_poh_supplement() {
  # Split Eurofox 3K POH Supplement
  eurofox_3k_poh_supplement_path="POH_Supplement_MTOW"
  eurofox_3k_poh_supplement="${dist_dir}/${eurofox_3k_poh_supplement_path}"
  eurofox_3k_poh_supplement_pdf="${eurofox_3k_poh_supplement}.pdf"
  if [ -f "${eurofox_3k_poh_supplement_pdf}" ]; then
    n_pages "${eurofox_3k_poh_supplement_pdf}"
  fi
}

function build_eurofox_3k_annex_01_151107() {
  # Split Eurofox 3K POH Supplement
  eurofox_3k_annex_01_151107_path="Annex_01-151107"
  eurofox_3k_annex_01_151107="${dist_dir}/${eurofox_3k_annex_01_151107_path}"
  eurofox_3k_annex_01_151107_pdf="${eurofox_3k_annex_01_151107}.pdf"
  if [ -f "${eurofox_3k_annex_01_151107_pdf}" ]; then
    n_pages "${eurofox_3k_annex_01_151107_pdf}"
  fi
}

function build_248300_manual() {
  # Split 24-8300 Manual
  foxbat_24_8300_manual_path="24-8300/foxbat-a22ls_24-8300"
  foxbat_24_8300_manual="${dist_dir}/${foxbat_24_8300_manual_path}"
  foxbat_24_8300_manual_pdf="${foxbat_24_8300_manual}.pdf"
  if [ -f "${foxbat_24_8300_manual_pdf}" ]; then
    n_pages ${foxbat_24_8300_manual_pdf}
  fi
}

function build_a22ls_2012_manual() {
  # Split A22LS 2012 Factory Manual
  a22ls_2012_manual_path="A-22LS_POH-S-N-146-R912iS"
  a22ls_2012_manual="${dist_dir}/${a22ls_2012_manual_path}"
  a22ls_2012_manual_pdf="${a22ls_2012_manual}.pdf"
  if [ -f "${a22ls_2012_manual_pdf}" ]; then
    n_pages ${a22ls_2012_manual_pdf}
  fi
}

function build_a22ls_2015_manual() {
  # Split A22LS 2012 Factory Manual
  a22ls_2015_manual_path="A22LS-POH-V5-Complete"
  a22ls_2015_manual="${dist_dir}/${a22ls_2015_manual_path}"
  a22ls_2015_manual_pdf="${a22ls_2015_manual}.pdf"
  if [ -f "${a22ls_2015_manual_pdf}" ]; then
    n_pages ${a22ls_2015_manual_pdf}
  fi
}

function build_232212_manual() {
  # Split Magnus Fusion 212 Manual
  magnus_fusion_232212_manual_path="23-2212/magnus-fusion-212_23-2212_manual"
  magnus_fusion_232212_manual="${dist_dir}/${magnus_fusion_232212_manual_path}"
  magnus_fusion_232212_manual_pdf="${magnus_fusion_232212_manual}.pdf"
  if [ -f "${magnus_fusion_232212_manual_pdf}" ]; then
    n_pages ${magnus_fusion_232212_manual_pdf}
  fi
}

function build_vhrmv_manual() {
  # Split VH-RMV Manual
  vh_rmv_manual_path="vh-rmv/vans-rv14_vh-rmv"
  vh_rmv_manual="${dist_dir}/${vh_rmv_manual_path}"
  vh_rmv_manual_pdf="${vh_rmv_manual}.pdf"
  if [ -f "${vh_rmv_manual_pdf}" ]; then
    n_pages ${vh_rmv_manual_pdf}
    split_pdf ${vh_rmv_manual_pdf} 1 ${vh_rmv_manual_path}_cover.pdf
    split_pdf ${vh_rmv_manual_pdf} 2-3 ${vh_rmv_manual_path}_equipment_list.pdf
    split_pdf ${vh_rmv_manual_pdf} 4-5 ${vh_rmv_manual_path}_performance_specifications.pdf
    split_pdf ${vh_rmv_manual_pdf} 7 ${vh_rmv_manual_path}_aerobatic_information.pdf
    split_pdf ${vh_rmv_manual_pdf} 8-9 ${vh_rmv_manual_path}_preflight_inspection.pdf
    split_pdf ${vh_rmv_manual_pdf} 10 ${vh_rmv_manual_path}_starting_engine.pdf
    split_pdf ${vh_rmv_manual_pdf} 11 ${vh_rmv_manual_path}_before_takeoff.pdf
    split_pdf ${vh_rmv_manual_pdf} 12 ${vh_rmv_manual_path}_takeoff.pdf
    split_pdf ${vh_rmv_manual_pdf} 13 ${vh_rmv_manual_path}_landing.pdf
    split_pdf ${vh_rmv_manual_pdf} 14 ${vh_rmv_manual_path}_performance.pdf
    split_pdf ${vh_rmv_manual_pdf} 15 ${vh_rmv_manual_path}_engine_information.pdf
    split_pdf ${vh_rmv_manual_pdf} 17-20 ${vh_rmv_manual_path}_emergency_procedures.pdf
    split_pdf ${vh_rmv_manual_pdf} 21-25 ${vh_rmv_manual_path}_weight_and_balance.pdf
  fi
}

function build_vhegy_manual() {
  # Split VH-EGY Manual
  vh_egy_manual_path="vh-egy/cessna-152_vh-egy"
  vh_egy_manual="${dist_dir}/${vh_egy_manual_path}"
  vh_egy_manual_pdf="${vh_egy_manual}.pdf"
  if [ -f "${vh_egy_manual_pdf}" ]; then
    n_pages ${vh_egy_manual_pdf}
    split_pdf ${vh_egy_manual_pdf} 1-2 ${vh_egy_manual_path}_certificate_of_registration.pdf
    split_pdf ${vh_egy_manual_pdf} 3 ${vh_egy_manual_path}_cover.pdf
    split_pdf ${vh_egy_manual_pdf} 4 ${vh_egy_manual_path}_performance_specifications.pdf
    split_pdf ${vh_egy_manual_pdf} 5 ${vh_egy_manual_path}_table_of_contents.pdf
    split_pdf ${vh_egy_manual_pdf} 6-13 ${vh_egy_manual_path}_section1_general.pdf
    split_pdf ${vh_egy_manual_pdf} 14-22 ${vh_egy_manual_path}_section2_limitations.pdf
    split_pdf ${vh_egy_manual_pdf} 23-36 ${vh_egy_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${vh_egy_manual_pdf} 37-57 ${vh_egy_manual_path}_section4_normal_procedures.pdf
    split_pdf ${vh_egy_manual_pdf} 58-77 ${vh_egy_manual_path}_section5_performance.pdf
    split_pdf ${vh_egy_manual_pdf} 78-98 ${vh_egy_manual_path}_section6_weight_and_balance.pdf
    split_pdf ${vh_egy_manual_pdf} 99-131 ${vh_egy_manual_path}_section7_airplane_and_systems_descriptions.pdf
    split_pdf ${vh_egy_manual_pdf} 132-144 ${vh_egy_manual_path}_section8_airplane_handling_and_maintenance.pdf
    split_pdf ${vh_egy_manual_pdf} 145-201 ${vh_egy_manual_path}_section9_supplements.pdf
    split_pdf ${vh_egy_manual_pdf} 202 ${vh_egy_manual_path}_stc_SA1000NW.pdf
  fi
}

function build_vhnkj_manual() {
  # Split VH-NKJ Manual
  vh_nkj_manual_path="vh-nkj/cessna-152_vh-nkj"
  vh_nkj_manual="${dist_dir}/${vh_nkj_manual_path}"
  vh_nkj_manual_pdf="${vh_nkj_manual}.pdf"
  if [ -f "${vh_nkj_manual_pdf}" ]; then
    n_pages ${vh_nkj_manual_pdf}
    split_pdf ${vh_nkj_manual_pdf} 1 ${vh_nkj_manual_path}_vhnkj_load_data_sheet.pdf
    split_pdf ${vh_nkj_manual_pdf} 5-6 ${vh_nkj_manual_path}_cover.pdf
    split_pdf ${vh_nkj_manual_pdf} 7 ${vh_nkj_manual_path}_congratulations.pdf
    split_pdf ${vh_nkj_manual_pdf} 8 ${vh_nkj_manual_path}_performance_specifications.pdf
    split_pdf ${vh_nkj_manual_pdf} 9 ${vh_nkj_manual_path}_table_of_contents.pdf
    split_pdf ${vh_nkj_manual_pdf} 10-17 ${vh_nkj_manual_path}_section1_general.pdf
    split_pdf ${vh_nkj_manual_pdf} 18-25 ${vh_nkj_manual_path}_section2_limitations.pdf
    split_pdf ${vh_nkj_manual_pdf} 26-40 ${vh_nkj_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${vh_nkj_manual_pdf} 41-61 ${vh_nkj_manual_path}_section4_normal_procedures.pdf
    split_pdf ${vh_nkj_manual_pdf} 62-79 ${vh_nkj_manual_path}_section5_performance.pdf
    split_pdf ${vh_nkj_manual_pdf} 80-99 ${vh_nkj_manual_path}_section6_weight_and_balance.pdf
    split_pdf ${vh_nkj_manual_pdf} 100-132 ${vh_nkj_manual_path}_section7_airplane_and_systems_descriptions.pdf
    split_pdf ${vh_nkj_manual_pdf} 133-145 ${vh_nkj_manual_path}_section8_airplane_handling_and_maintenance.pdf
    split_pdf ${vh_nkj_manual_pdf} 146-205 ${vh_nkj_manual_path}_section9_supplements.pdf
  fi
}

function build_cessna172n_manual() {
  # Split Cessna 172N Factory Manual
  cessna172n_manual_path="Cessna_172_C172N-1978-POH-S1to7-scanned"
  cessna172n_manual="${dist_dir}/${cessna172n_manual_path}"
  cessna172n_manual_pdf="${cessna172n_manual}.pdf"
  if [ -f "${cessna172n_manual_pdf}" ]; then
    n_pages ${cessna172n_manual_pdf}
  fi
}

function build_cessna172r_manual() {
  # Split Cessna 172R Factory Manual
  cessna172r_manual_path="skyhawk_pim-20130523"
  cessna172r_manual="${dist_dir}/${cessna172r_manual_path}"
  cessna172r_manual_pdf="${cessna172r_manual}.pdf"
  if [ -f "${cessna172r_manual_pdf}" ]; then
    n_pages ${cessna172r_manual_pdf}
    split_pdf ${cessna172r_manual_pdf} 1 ${cessna172r_manual_path}_notice.pdf
    split_pdf ${cessna172r_manual_pdf} 2-3 ${cessna172r_manual_path}_performance_specifications.pdf
    split_pdf ${cessna172r_manual_pdf} 5 ${cessna172r_manual_path}_cover.pdf
    split_pdf ${cessna172r_manual_pdf} 7 ${cessna172r_manual_path}_table_of_contents.pdf
    split_pdf ${cessna172r_manual_pdf} 9-38 ${cessna172r_manual_path}_section1_general.pdf
    split_pdf ${cessna172r_manual_pdf} 39-66 ${cessna172r_manual_path}_section2_operating_limitations.pdf
    split_pdf ${cessna172r_manual_pdf} 67-105 ${cessna172r_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${cessna172r_manual_pdf} 107-154 ${cessna172r_manual_path}_section4_normal_procedures.pdf
    split_pdf ${cessna172r_manual_pdf} 155-176 ${cessna172r_manual_path}_section5_performance.pdf
    split_pdf ${cessna172r_manual_pdf} 177-200 ${cessna172r_manual_path}_section6_weight_and_balance_equipment.pdf
    split_pdf ${cessna172r_manual_pdf} 201-280 ${cessna172r_manual_path}_section7_airplane_and_systems_descriptions.pdf
    split_pdf ${cessna172r_manual_pdf} 281-305 ${cessna172r_manual_path}_section8_airplane_handling_service_and_maintenance.pdf
    split_pdf ${cessna172r_manual_pdf} 307-349 ${cessna172r_manual_path}_section9_supplements.pdf
  fi
}

function build_cessna172s_manual() {
  # Split Cessna 172S Factory Manual
  cessna172s_manual_path="skyhawksp_pim-20130523"
  cessna172s_manual="${dist_dir}/${cessna172s_manual_path}"
  cessna172s_manual_pdf="${cessna172s_manual}.pdf"
  if [ -f "${cessna172s_manual_pdf}" ]; then
    n_pages ${cessna172s_manual_pdf}
    split_pdf ${cessna172s_manual_pdf} 1 ${cessna172s_manual_path}_notice.pdf
    split_pdf ${cessna172s_manual_pdf} 2-3 ${cessna172s_manual_path}_performance_specifications.pdf
    split_pdf ${cessna172s_manual_pdf} 5 ${cessna172s_manual_path}_cover.pdf
    split_pdf ${cessna172s_manual_pdf} 7 ${cessna172s_manual_path}_table_of_contents.pdf
    split_pdf ${cessna172s_manual_pdf} 9-38 ${cessna172s_manual_path}_section1_general.pdf
    split_pdf ${cessna172s_manual_pdf} 39-66 ${cessna172s_manual_path}_section2_operating_limitations.pdf
    split_pdf ${cessna172s_manual_pdf} 67-105 ${cessna172s_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${cessna172s_manual_pdf} 107-154 ${cessna172s_manual_path}_section4_normal_procedures.pdf
    split_pdf ${cessna172s_manual_pdf} 155-178 ${cessna172s_manual_path}_section5_performance.pdf
    split_pdf ${cessna172s_manual_pdf} 179-202 ${cessna172s_manual_path}_section6_weight_and_balance_equipment.pdf
    split_pdf ${cessna172s_manual_pdf} 203-282 ${cessna172s_manual_path}_section7_airplane_and_systems_descriptions.pdf
    split_pdf ${cessna172s_manual_pdf} 283-307 ${cessna172s_manual_path}_section8_airplane_handling_service_and_maintenance.pdf
    split_pdf ${cessna172s_manual_pdf} 308-351 ${cessna172s_manual_path}_section9_supplements.pdf
  fi
}

function build_cessna182p_manual() {
  # Split Cessna 182P Factory Manual
  cessna182p_manual_path="cessna-182p"
  cessna182p_manual="${dist_dir}/${cessna182p_manual_path}"
  cessna182p_manual_pdf="${cessna182p_manual}.pdf"
  if [ -f "${cessna182p_manual_pdf}" ]; then
    n_pages ${cessna182p_manual_pdf}
    split_pdf ${cessna182p_manual_pdf} 1 ${cessna182p_manual_path}_cover.pdf
    split_pdf ${cessna182p_manual_pdf} 2 ${cessna182p_manual_path}_list_of_effective_pages.pdf
    split_pdf ${cessna182p_manual_pdf} 3 ${cessna182p_manual_path}_congratulations.pdf
    split_pdf ${cessna182p_manual_pdf} 4 ${cessna182p_manual_path}_performance_specifications.pdf
    split_pdf ${cessna182p_manual_pdf} 5 ${cessna182p_manual_path}_table_of_contents.pdf
    split_pdf ${cessna182p_manual_pdf} 7-14 ${cessna182p_manual_path}_section1_general.pdf
    split_pdf ${cessna182p_manual_pdf} 15-25 ${cessna182p_manual_path}_section2_limitations.pdf
    split_pdf ${cessna182p_manual_pdf} 27-41 ${cessna182p_manual_path}_section3_emergency_procedures.pdf
    split_pdf ${cessna182p_manual_pdf} 43-64 ${cessna182p_manual_path}_section4_normal_procedures.pdf
    split_pdf ${cessna182p_manual_pdf} 65-91 ${cessna182p_manual_path}_section5_performance.pdf
    split_pdf ${cessna182p_manual_pdf} 93-117 ${cessna182p_manual_path}_section6_weight_and_balance_equipment.pdf
    split_pdf ${cessna182p_manual_pdf} 119-158 ${cessna182p_manual_path}_section7_airplane_and_systems_descriptions.pdf
    split_pdf ${cessna182p_manual_pdf} 159-172 ${cessna182p_manual_path}_section8_airplane_handling_service_and_maintenance.pdf
    split_pdf ${cessna182p_manual_pdf} 173-258 ${cessna182p_manual_path}_section9_supplements.pdf
  fi
}

function build_vhtin_manual() {
  # Split VH-TIN Manual
  vh_tin_manual_path="vh-tin/cessna-182p_vh-tin"
  vh_tin_manual="${dist_dir}/${vh_tin_manual_path}"
  vh_tin_manual_pdf="${vh_tin_manual}.pdf"
  if [ -f "${vh_tin_manual_pdf}" ]; then
    n_pages ${vh_tin_manual_pdf}
    split_pdf ${vh_tin_manual_pdf} 1 ${vh_tin_manual_path}_amendment_record_sheet.pdf
    split_pdf ${vh_tin_manual_pdf} 2-13 ${vh_tin_manual_path}_certificate_of_registration.pdf
    split_pdf ${vh_tin_manual_pdf} 14-15 ${vh_tin_manual_path}_cover.pdf
    split_pdf ${vh_tin_manual_pdf} 16-26 ${vh_tin_manual_path}_amendment_record_sheets.pdf
    split_pdf ${vh_tin_manual_pdf} 27-27 ${vh_tin_manual_path}_introduction.pdf
    split_pdf ${vh_tin_manual_pdf} 28-30 ${vh_tin_manual_path}_list_of_contents.pdf
    split_pdf ${vh_tin_manual_pdf} 31-31 ${vh_tin_manual_path}_definitions.pdf
    split_pdf ${vh_tin_manual_pdf} 32-34 ${vh_tin_manual_path}_section1_general_aeroplane_particulars.pdf
    split_pdf ${vh_tin_manual_pdf} 35-38 ${vh_tin_manual_path}_section2_operating_limitations.pdf
    split_pdf ${vh_tin_manual_pdf} 39-41 ${vh_tin_manual_path}_section3_handling.pdf
    split_pdf ${vh_tin_manual_pdf} 42-44 ${vh_tin_manual_path}_section4_performance.pdf
    split_pdf ${vh_tin_manual_pdf} 45-48 ${vh_tin_manual_path}_section5_instrument_and_equipment_installations.pdf
    split_pdf ${vh_tin_manual_pdf} 49-57 ${vh_tin_manual_path}_section6_loading_data.pdf
    split_pdf ${vh_tin_manual_pdf} 58-92 ${vh_tin_manual_path}_section7_supplements.pdf
  fi
}

function build_rotax_912_manual() {
  # Split Rotax 912 Manual
  rotax_912_manual_path="OM_912_Series_ED4_R0"
  rotax_912_manual="${dist_dir}/${rotax_912_manual_path}"
  rotax_912_manual_pdf="${rotax_912_manual}.pdf"
  if [ -f "${rotax_912_manual_pdf}" ]; then
    n_pages ${rotax_912_manual_pdf}
    split_pdf ${rotax_912_manual_pdf} 1 ${rotax_912_manual_path}_cover.pdf
    split_pdf ${rotax_912_manual_pdf} 2 ${rotax_912_manual_path}_rotax.pdf
    split_pdf ${rotax_912_manual_pdf} 3 ${rotax_912_manual_path}_table_of_contents.pdf
    split_pdf ${rotax_912_manual_pdf} 5 ${rotax_912_manual_path}_introduction.pdf
    split_pdf ${rotax_912_manual_pdf} 7-8 ${rotax_912_manual_path}_list_of_effective_pages.pdf
    split_pdf ${rotax_912_manual_pdf} 9-10 ${rotax_912_manual_path}_table_of_amendments.pdf
    split_pdf ${rotax_912_manual_pdf} 11-29 ${rotax_912_manual_path}_general_note.pdf
    split_pdf ${rotax_912_manual_pdf} 31-41 ${rotax_912_manual_path}_operating_instructions.pdf
    split_pdf ${rotax_912_manual_pdf} 43-53 ${rotax_912_manual_path}_standard_operation.pdf
    split_pdf ${rotax_912_manual_pdf} 55-62 ${rotax_912_manual_path}_abnormal_operation.pdf
    split_pdf ${rotax_912_manual_pdf} 63-69 ${rotax_912_manual_path}_performance_and_fuel_consumption.pdf
    split_pdf ${rotax_912_manual_pdf} 71 ${rotax_912_manual_path}_weights.pdf
    split_pdf ${rotax_912_manual_pdf} 73-80 ${rotax_912_manual_path}_system_description.pdf
    split_pdf ${rotax_912_manual_pdf} 81-83 ${rotax_912_manual_path}_preservation_and_storage.pdf
    split_pdf ${rotax_912_manual_pdf} 85-86 ${rotax_912_manual_path}_supplement.pdf
    split_pdf ${rotax_912_manual_pdf} 87-88 ${rotax_912_manual_path}_index.pdf
    split_pdf ${rotax_912_manual_pdf} 89 ${rotax_912_manual_path}_list_of_figures.pdf
    split_pdf ${rotax_912_manual_pdf} 91 ${rotax_912_manual_path}_back_cover.pdf
  fi
}

function build_dynon_sv_d700_sv_d1000_sv_d1000t_manual() {
  # Split Dynon SV-D700/SV-D1000/SV-D1000T
  dynon_sv_d700_sv_d1000_sv_d1000t_manual_path="sv-d700_sv-d1000_sv-d1000t/SkyView_System_Installation_Guide-Rev_W_v14_0"
  dynon_sv_d700_sv_d1000_sv_d1000t_manual="${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}"
  dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf="${dynon_sv_d700_sv_d1000_sv_d1000t_manual}.pdf"
  if [ -f "${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf}" ]; then
    n_pages ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf}
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 1 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_cover.pdf # Cover
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 3-4 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_changes_and_contacts.pdf # Contact & Changes
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 5-11 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_table_of_contents.pdf # Table of Contents
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 13-18 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_introduction.pdf # 1 Introduction
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 19-34 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_system_planning.pdf # 2 System Planning
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 35-53 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_basic_skyview_display_operation.pdf # 3 Basic SkyView Display Operation
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 55-87 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-d700_sv-d1000_sv-d1000t_installation_and_onfiguration.pdf # 4 SV-D700 / SV-D1000 / SV-D1000T Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 89-106 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-adahrs-200_201_installation_and_configuration.pdf # 5 SV-ADAHRS-200/201 Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 107-114 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-map-270_navigation_mapping_license_purchase_and_setup.pdf # 6 SV-MAP-270 Navigation Mapping License Purchase and Setup
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 115-191 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-ems-220_221_installation_and_configuration.pdf # 7 SV-EMS-220/221 Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 193-205 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-gps-250_sv-gps-2020_gps_receiver_installation_and_configuration.pdf # 8 SV-GPS-250 / SV-GPS-2020 GPS Receiver Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 207-213 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-bat-320_installation.pdf # 9 SV-BAT-320 Installation
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 215-236 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_autopilot_servo_installation_configuration_and_calibration.pdf # 10 Autopilot Servo Installation, Configuration, and Calibration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 237-275 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-xpndr-261_262_installation_configuration_and_testing.pdf # 11 SV-XPNDR-261 / 262 Installation, Configuration, and Testing
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 277-288 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-arinc-429_installation_and_configuration.pdf # 12 SV-ARINC-429 Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 289-293 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_vertical_power_vp-x_ntegration_and_onfiguration.pdf # 13 Vertical Power VP-X Integration and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 295-304 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-adsb-470_installation_configuration_and_testing.pdf # 14 SV-ADSB-470 Installation, Configuration, and Testing
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 305-319 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_accessory_installation_and_configuration.pdf # 15 Accessory Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 321-339 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-com-c25_installation_configuration_and_testing.pdf # 16 SV-COM-C25 Installation, Configuration, and Testing
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 341-362 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-com-x83_installation_configuration_and_testing.pdf # 17 SV-COM-X83 Installation, Configuration, and Testing
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 363-371 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-ap-panel_installation.pdf # 18 SV-AP-PANEL Installation
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 373-375 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-knob-panel_installation.pdf # 19 SV-KNOB-PANEL Installation
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 377-385 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-mag-236_installation_and_configuration.pdf # 20 SV-MAG-236 Installation and Configuration
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 387-399 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_a_maintenance_and_troubleshooting.pdf # 21 Appendix A: Maintenance and Troubleshooting
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 401-413 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_b_specifications.pdf # 22 Appendix B: Specifications
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 415-438 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_c_wiring_and_electrical_connections.pdf # 23 Appendix C: Wiring and Electrical Connections
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 439-440 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_d_sv-ems-220_221_sensor_input_mapping_worksheet.pdf # 24 Appendix D: SV-EMS-220/221 Sensor Input Mapping Worksheet
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 441-453 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_e_serial_data_output.pdf # 25 Appendix E: Serial Data Output
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 455-458 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_f_user_data_logs.pdf # 26 Appendix F: User Data Logs
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 459 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_g_kavlico_pressure_sensor_part_numbers.pdf # 27 Appendix G: Kavlico Pressure Sensor Part Numbers
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 461-463 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_h_sensor_debug_data.pdf # 28 Appendix H: Sensor Debug Data
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 465-489 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_i_skyview_system_installation_guide_revision_history.pdf # 29 Appendix I: SkyView System Installation Guide Revision History
    split_pdf ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} 491-496 ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_j_checklists.pdf # 30 Appendix J: Checklists
  fi
}

function build_dynon_flightdek_d180_manual() {
  # Split Dynon FlightDEK D-180
  dynon_flightdek_d180_manual_path="flightdek-d180/FlightDEK-D180_Pilot's_User_Guide_Rev_H"
  dynon_flightdek_d180_manual="${dist_dir}/${dynon_flightdek_d180_manual_path}"
  dynon_flightdek_d180_manual_pdf="${dynon_flightdek_d180_manual}.pdf"
  if [ -f "${dynon_flightdek_d180_manual_pdf}" ]; then
    n_pages ${dynon_flightdek_d180_manual_pdf}
    split_pdf ${dynon_flightdek_d180_manual_pdf} 1-2 ${dynon_flightdek_d180_manual_path}_cover.pdf # Cover
    split_pdf ${dynon_flightdek_d180_manual_pdf} 2-3 ${dynon_flightdek_d180_manual_path}_contact_information.pdf # Contact Information
    split_pdf ${dynon_flightdek_d180_manual_pdf} 4-7 ${dynon_flightdek_d180_manual_path}_table_of_contacts.pdf # Table of Contents
    split_pdf ${dynon_flightdek_d180_manual_pdf} 8-10 ${dynon_flightdek_d180_manual_path}_introduction.pdf # Section 1 Introduction
    split_pdf ${dynon_flightdek_d180_manual_pdf} 11-15 ${dynon_flightdek_d180_manual_path}_product_overview.pdf # Section 2 Product Overview
    split_pdf ${dynon_flightdek_d180_manual_pdf} 16-23 ${dynon_flightdek_d180_manual_path}_product_operation.pdf # Section 3 Product Operation
    split_pdf ${dynon_flightdek_d180_manual_pdf} 24-37 ${dynon_flightdek_d180_manual_path}_available_pages.pdf # Section 4 Available Pages
    split_pdf ${dynon_flightdek_d180_manual_pdf} 38-52 ${dynon_flightdek_d180_manual_path}_efis_operation.pdf # Section 5 EFIS Operation
    split_pdf ${dynon_flightdek_d180_manual_pdf} 53-62 ${dynon_flightdek_d180_manual_path}_hsi_operation.pdf # Section 6 HSI Operation
    split_pdf ${dynon_flightdek_d180_manual_pdf} 63-79 ${dynon_flightdek_d180_manual_path}_autopilot_operation.pdf # Section 7 Autopilot Operation
    split_pdf ${dynon_flightdek_d180_manual_pdf} 80-85 ${dynon_flightdek_d180_manual_path}_alerts.pdf # Section 8 Alerts
    split_pdf ${dynon_flightdek_d180_manual_pdf} 86-89 ${dynon_flightdek_d180_manual_path}_ems_monitoring_functions.pdf # Section 9 EMS Monitoring Functions
    split_pdf ${dynon_flightdek_d180_manual_pdf} 90-96 ${dynon_flightdek_d180_manual_path}_ems_operation.pdf # Section 10 EMS Operation
    split_pdf ${dynon_flightdek_d180_manual_pdf} 97-108 ${dynon_flightdek_d180_manual_path}_appendix.pdf # Appendix
  fi
}

function build_dynon_efis_d100_manual() {
  # Split Dynon EFIS D-100
  dynon_efis_d100_manual_path="efis-d100/EFIS-D100_Pilot's_User_Guide_Rev_H"
  dynon_efis_d100_manual="${dist_dir}/${dynon_efis_d100_manual_path}"
  dynon_efis_d100_manual_pdf="${dynon_efis_d100_manual}.pdf"
  if [ -f "${dynon_efis_d100_manual_pdf}" ]; then
    n_pages ${dynon_efis_d100_manual_pdf}
    split_pdf ${dynon_efis_d100_manual_pdf} 1 ${dynon_efis_d100_manual_path}_cover.pdf # Cover
    split_pdf ${dynon_efis_d100_manual_pdf} 2-3 ${dynon_efis_d100_manual_path}_contact_information.pdf # Contact Information
    split_pdf ${dynon_efis_d100_manual_pdf} 4-6 ${dynon_efis_d100_manual_path}_table_of_contents.pdf # Table of Contents
    split_pdf ${dynon_efis_d100_manual_pdf} 7-9 ${dynon_efis_d100_manual_path}_introduction.pdf # Section 1 Introduction
    split_pdf ${dynon_efis_d100_manual_pdf} 10-14 ${dynon_efis_d100_manual_path}_product_overview.pdf # Section 2 Product Overview
    split_pdf ${dynon_efis_d100_manual_pdf} 15-22 ${dynon_efis_d100_manual_path}_product_operation.pdf # Section 3 Product Operation
    split_pdf ${dynon_efis_d100_manual_pdf} 23-33 ${dynon_efis_d100_manual_path}_available_pages.pdf # Section 4 Available Pages
    split_pdf ${dynon_efis_d100_manual_pdf} 34-49 ${dynon_efis_d100_manual_path}_efis_operation.pdf # Section 5 EFIS Operation
    split_pdf ${dynon_efis_d100_manual_pdf} 50-59 ${dynon_efis_d100_manual_path}_hsi_operation.pdf # Section 6 HSI Operation
    split_pdf ${dynon_efis_d100_manual_pdf} 60-76 ${dynon_efis_d100_manual_path}_autopilot_operation.pdf # Section 7 Autopilot Operation
    split_pdf ${dynon_efis_d100_manual_pdf} 77-80 ${dynon_efis_d100_manual_path}_alerts.pdf # Section 8 Alerts
    split_pdf ${dynon_efis_d100_manual_pdf} 81-89 ${dynon_efis_d100_manual_path}_appendix.pdf # Appendix
  fi
}

function build_garmin_g500_g600_manual() {
  # Split Garmin G500/G600
  garmin_g500_g600_manual_path="d500_d600/190-00601-02_K"
  garmin_g500_g600_manual="${dist_dir}/${garmin_g500_g600_manual_path}"
  garmin_g500_g600_manual_pdf="${garmin_g500_g600_manual}.pdf"
  if [ -f "${garmin_g500_g600_manual_pdf}" ]; then
    n_pages ${garmin_g500_g600_manual_pdf}
    split_pdf ${garmin_g500_g600_manual_pdf} 1 ${garmin_g500_g600_manual_path}_cover.pdf # Cover
    split_pdf ${garmin_g500_g600_manual_pdf} 2-10 ${garmin_g500_g600_manual_path}_foreword.pdf # Foreword
    split_pdf ${garmin_g500_g600_manual_pdf} 11-16 ${garmin_g500_g600_manual_path}_table_of_contents.pdf # Table of Contents
    split_pdf ${garmin_g500_g600_manual_pdf} 17-42 ${garmin_g500_g600_manual_path}_system_overview.pdf # Section 1 System Overview
    split_pdf ${garmin_g500_g600_manual_pdf} 43-82 ${garmin_g500_g600_manual_path}_primary_flight_display.pdf # Section 2 Primary Flight Display
    split_pdf ${garmin_g500_g600_manual_pdf} 83-158 ${garmin_g500_g600_manual_path}_multi_function_display.pdf # Section 3 Multi-Function Display
    split_pdf ${garmin_g500_g600_manual_pdf} 159-316 ${garmin_g500_g600_manual_path}_hazard_avoidance.pdf # Section 4 Hazard Avoidance
    split_pdf ${garmin_g500_g600_manual_pdf} 317-368 ${garmin_g500_g600_manual_path}_additional_features.pdf # Section 5 Additional Features
    split_pdf ${garmin_g500_g600_manual_pdf} 369-384 ${garmin_g500_g600_manual_path}_annunciations_and_alerts.pdf # Section 6 Annunciations and Alerts
    split_pdf ${garmin_g500_g600_manual_pdf} 385-392 ${garmin_g500_g600_manual_path}_symbols.pdf # Section 7 Symbols
    split_pdf ${garmin_g500_g600_manual_pdf} 393-400 ${garmin_g500_g600_manual_path}_glossary.pdf # Section 8 Glossary
    split_pdf ${garmin_g500_g600_manual_pdf} 401-408 ${garmin_g500_g600_manual_path}_appendix_a.pdf # Appendix A
    split_pdf ${garmin_g500_g600_manual_pdf} 409-418 ${garmin_g500_g600_manual_path}_index.pdf # Index
  fi
}

function build_avmap_ekp_iv_manual() {
  # Split AvMap EKP IV/Pro
  avmap_ekp_iv_manual_path="ekpiv_ekpiv_pro/EKP_pro_inglese_162"
  avmap_ekp_iv_manual="${dist_dir}/${avmap_ekp_iv_manual_path}"
  avmap_ekp_iv_manual_pdf="${avmap_ekp_iv_manual}.pdf"
  if [ -f "${avmap_ekp_iv_manual_pdf}" ]; then
    n_pages ${avmap_ekp_iv_manual_pdf}
    split_pdf ${avmap_ekp_iv_manual_pdf} 1 ${avmap_ekp_iv_manual_path}_cover.pdf # Cover
    split_pdf ${avmap_ekp_iv_manual_pdf} 2-3 ${avmap_ekp_iv_manual_path}_foreword.pdf # Foreword
    split_pdf ${avmap_ekp_iv_manual_pdf} 4-8 ${avmap_ekp_iv_manual_path}_table_of_contents.pdf # Table of Contents
    split_pdf ${avmap_ekp_iv_manual_pdf} 5-15 ${avmap_ekp_iv_manual_path}_introduction.pdf # Section 1 Introduction
    split_pdf ${avmap_ekp_iv_manual_pdf} 16-21 ${avmap_ekp_iv_manual_path}_the_basics.pdf # Section 2 The Basics
    split_pdf ${avmap_ekp_iv_manual_pdf} 22-35 ${avmap_ekp_iv_manual_path}_the_moving_maps.pdf # Section 3 The Moving Maps
    split_pdf ${avmap_ekp_iv_manual_pdf} 36-37 ${avmap_ekp_iv_manual_path}_navigation_and_location.pdf # Section 4 Navigation and Location
    split_pdf ${avmap_ekp_iv_manual_pdf} 38-39 ${avmap_ekp_iv_manual_path}_the_hsi_screen.pdf # Section 5 The HSI Screen
    split_pdf ${avmap_ekp_iv_manual_pdf} 40-44 ${avmap_ekp_iv_manual_path}_flight_plan.pdf # Section 6 Flight Plan
    split_pdf ${avmap_ekp_iv_manual_pdf} 45-47 ${avmap_ekp_iv_manual_path}_the_global_positioning_system.pdf # Section 7 The Global Positioning System
    split_pdf ${avmap_ekp_iv_manual_pdf} 48-51 ${avmap_ekp_iv_manual_path}_waypoint_and_database.pdf # Section 8 Waypoint and Database
    split_pdf ${avmap_ekp_iv_manual_pdf} 52-53 ${avmap_ekp_iv_manual_path}_approach_data_procedures.pdf # Section 9 Approach Data Procedures
    split_pdf ${avmap_ekp_iv_manual_pdf} 54-59 ${avmap_ekp_iv_manual_path}_calculator.pdf # Section 10 Calculator
    split_pdf ${avmap_ekp_iv_manual_pdf} 60-61 ${avmap_ekp_iv_manual_path}_the_checklists.pdf # Section 11 The Checklists
    split_pdf ${avmap_ekp_iv_manual_pdf} 62-63 ${avmap_ekp_iv_manual_path}_simulator.pdf # Section 12 Simulator
    split_pdf ${avmap_ekp_iv_manual_pdf} 64-65 ${avmap_ekp_iv_manual_path}_the_communication.pdf # Section 13 The Communication
    split_pdf ${avmap_ekp_iv_manual_pdf} 66-75 ${avmap_ekp_iv_manual_path}_the_system_setup_menu.pdf # Section 14 The System Setup Menu
    split_pdf ${avmap_ekp_iv_manual_pdf} 76-82 ${avmap_ekp_iv_manual_path}_operating_requirements.pdf # Section 15 Operating Requirements
    split_pdf ${avmap_ekp_iv_manual_pdf} 83 ${avmap_ekp_iv_manual_path}_ekp_iv_pro.pdf # Section 16 EKP IV Pro
    split_pdf ${avmap_ekp_iv_manual_pdf} 84-95 ${avmap_ekp_iv_manual_path}_appendices.pdf # Appendices
  fi
}

function make_page1() {
  pdf_files=$(cd ${dist_dir} > /dev/null && find . -type f -name "*.pdf")

  OIFS=$IFS
  IFS=$'\n'
  for pdf_file in $pdf_files
  do
    basename=$(basename -- "${pdf_file}")
    dirname=$(dirname -- "${pdf_file}")
    filename="${basename%.*}"

    page1_pdf_dir=${dist_dir}/${dirname}
    page1_pdf="${page1_pdf_dir}/${filename}_page1.pdf"

    mkdir -p "${page1_pdf_dir}"

    gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${dist_dir}/${pdf_file}

    page1_pdf_png="${page1_pdf}.png"

    convert ${page1_pdf} ${page1_pdf_png}

    for size in 600 450 350 250 150 100 65 45
    do
      page1_pdf_png_size="${page1_pdf}-${size}.png"
      convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
    done
  done
  IFS="$OIFS"
}

function build_manuals() {
  build_vhysa_normal_procedures
  build_vhysa_emergency_procedures
  build_vhnqx_manual
  build_aquila_a210_manual
  build_vhbne_manual
  build_sling2_factory_manual
  build_244844_manual
  build_245350_manual
  build_248881_manual
  build_eurofox_3k_factory_manual
  build_eurofox_3k_maintenance_manual
  build_eurofox_3k_poh_supplement
  build_eurofox_3k_annex_01_151107
  build_248300_manual
  build_a22ls_2012_manual
  build_a22ls_2015_manual
  build_232212_manual
  build_vhrmv_manual
  build_vhegy_manual
  build_vhnkj_manual
  build_cessna172n_manual
  build_cessna172r_manual
  build_cessna172s_manual
  build_cessna182p_manual
  build_vhtin_manual
  build_rotax_912_manual
  build_dynon_sv_d700_sv_d1000_sv_d1000t_manual
  build_dynon_flightdek_d180_manual
  build_dynon_efis_d100_manual
  build_garmin_g500_g600_manual
  build_avmap_ekp_iv_manual
}

build_manuals
make_page1
archive_n_pages
